Feature: Cart management
  Adding and removing products from the cart

  Scenario: Add one product in the cart
    Given I am logged in
    When I go to the Home page
    And I navigate to category "Art"
    And I navigate to product "Affiche encadrée The best is yet to come"
    And I add to cart
    Then The cart contains
      | Product                                  | Number | Dimension | Size | Color |
      | Affiche encadrée The best is yet to come |      1 | 40x60cm   |      |       |

  Scenario: Add two products in the cart
    Given I am logged in
    When I go to the Home page
    And I navigate to category "Art"
    And I navigate to product "Affiche encadrée The best is yet to come"
    And I add to cart
    And I go to the Home page
    And I navigate to category "Art"
    And I navigate to product "Illustration vectorielle Renard"
    And I add to cart
    Then The cart contains
      | Product                                  | Number | Dimension | Size | Color |
      | Affiche encadrée The best is yet to come |      1 | 40x60cm   |      |       |
      | Illustration vectorielle Renard          |      1 |           |      |       |

  Scenario: Add twice a product to the cart
    Given I am logged in
    When I go to the Home page
    And I navigate to subcategory "Hommes"
    And I navigate to product "T-shirt imprimé colibri"
    And I add to cart
    And I go to the Home page
    And I navigate to subcategory "Hommes"
    And I navigate to product "T-shirt imprimé colibri"
    And I add to cart
    Then The cart contains
      | Product                 | Number | Dimension | Size | Color |
      | T-shirt imprimé colibri |      2 |           | S    | Blanc |
