package prestashoptest.pageobjects;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import prestashoptest.datatypes.Gender;
import prestashoptest.seleniumtools.PageObjectBase;

/**
 * Personal information page
 *
 */
public class IdentityPageObject extends PageObjectBase {

    private final static DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("d/MM/yyyy");

    public IdentityPageObject() {
        super("identite");
    }

    /**
     * Fill the gender field
     * If the gender is undefined, the field is untouched.
     *
     * @param gender
     */
    public void fillGender(final Gender gender) {
        if (gender.equals(Gender.UNDEFINED)) {
            return;
        }
        final String id = (gender.equals(Gender.MALE)) ? "field-id_gender-1"
                                                       : "field-id_gender-2";
        clickElement(SelectBy.ID, id);
    }

    /**
     * Get the gender field value
     *
     * @return gender
     */
    public Gender getGender() {
        if (isCheckBoxSelected(SelectBy.ID, "field-id_gender-1")) {
            return Gender.MALE;
        }
        if (isCheckBoxSelected(SelectBy.ID, "field-id_gender-2")) {
            return Gender.FEMALE;
        }
        return Gender.UNDEFINED;
    }

    /**
     * Fill the first name field
     *
     * @param firstName
     */
    public void fillFirstName(final String firstName) {
        fillFieldValue(SelectBy.ID, "field-firstname", firstName);
    }

    /**
     * Get the first name field value
     *
     * @return first name
     */
    public String getFirstName() {
        return getFieldValue(SelectBy.ID, "field-firstname");
    }

    /**
     * Fill the last name field
     *
     * @param lastName
     */
    public void fillLastName(final String lastName) {
        fillFieldValue(SelectBy.ID, "field-lastname", lastName);
    }

    /**
     * Get the last name field value
     *
     * @return last name
     */
    public String getLastName() {
        return getFieldValue(SelectBy.ID, "field-lastname");
    }

    /**
     * Fill the email field
     *
     * @param email
     */
    public void fillEmail(final String email) {
        fillFieldValue(SelectBy.ID, "field-email", email);
    }

    /**
     * Get the email field value
     *
     * @return email
     */
    public String getEmail() {
        return getFieldValue(SelectBy.ID, "field-email");
    }

    /**
     * Fill the password field
     *
     * @param password
     */
    public void fillPassword(final String password) {
        fillFieldValue(SelectBy.ID, "field-password", password);
    }

    /**
     * Fill the birth date field
     *
     * @param birthDate
     */
    public void fillBirthDate(final LocalDate birthDate) {
        fillFieldValue(SelectBy.ID, "field-birthday", birthDate.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
    }

    /**
     * Get the birth date field value
     *
     * @return birth date
     */
    public LocalDate getBirthDate() {
        final String birthDateStr = getFieldValue(SelectBy.ID, "field-birthday");
        return LocalDate.parse(birthDateStr, DATE_FORMATTER);
    }

    /**
     * Approve for partner offers
     */
    public void acceptPartnerOffers() {
        clickElement(SelectBy.NAME, "optin");
    }

    /**
     * Get the partner approval field value
     *
     * @return partner approval
     */
    public boolean doesAcceptPartnerOffers() {
        return isCheckBoxSelected(SelectBy.NAME, "optin");
    }

    /**
     * Approve the customer privacy policy
     */
    public void acceptPrivacyPolicy() {
        clickElement(SelectBy.NAME, "customer_privacy");
    }

    /**
     * Get the customer privacy field value
     *
     * @return customer privacy
     */
    public boolean doesAcceptPrivacyPolicy() {
        return isCheckBoxSelected(SelectBy.NAME, "customer_privacy");
    }

    /**
     * Sign up for the newsletter
     */
    public void acceptNewsletter() {
        clickElement(SelectBy.NAME, "newsletter");
    }

    /**
     * Get the newsletter field value
     *
     * @return newsletter
     */
    public boolean doesAcceptNewsletter() {
        return isCheckBoxSelected(SelectBy.NAME, "newsletter");
    }

    /**
     * Approve the GDPR policy
     */
    public void acceptGdpr() {
        clickElement(SelectBy.NAME, "psgdpr");
    }


    /**
     * Get the GDPR policy field value
     *
     * @return GDPR policy
     */
    public boolean doesAcceptGdpr() {
        return isCheckBoxSelected(SelectBy.NAME, "psgdpr");
    }

    /**
     * Initiate the account modification
     */
    public void submitForm() {
        clickElement(SelectBy.XPATH, "//*[@id=\"customer-form\"]/footer/button");
    }}
