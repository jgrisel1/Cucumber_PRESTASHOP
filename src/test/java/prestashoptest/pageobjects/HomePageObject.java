package prestashoptest.pageobjects;

import prestashoptest.seleniumtools.PageObjectBase;

/**
 * Home page
 */
public class HomePageObject extends PageObjectBase {

    public HomePageObject() {
        super("");
    }

    /**
     * Return true is the use is logged in, false otherwise
     *
     * @return
     */
    public boolean isSignedIn() {
       final String title = getElementAttribute(SelectBy.XPATH, "//a[contains(@href,'/mon-compte')]", "title");
       if (title.equals("Voir mon compte client")) {
           return true;
       }
       if (title.equals("Identifiez-vous")) {
           return false;
       }
       throw new IllegalStateException("Unexpected title for (de)connection link: " + title);
    }

    /**
     * Return the displayed user name
     *
     * @return
     */
    public String getDisplayedUserName() {
        return getElementText(SelectBy.XPATH, "//a[contains(@href,'/mon-compte')]/span");
    }

    /**
     * Sign out
     */
    public void signOut() {
        clickElement(SelectBy.LINKTEXTEXTRACT, "Déconnexion");
    }

    /**
     * Select a category
     *
     * @param category
     */
    public void selectCategory(final String category) {
        clickElement(SelectBy.XPATH, "//a[contains(text(),'" + category + "')]");
    }

    /**
     * Select a subcategory
     *
     * @param subcategory
     */
    public void selectSubcategory(final String subcategory) {
        final String subcategoryXPath = "//a[normalize-space(text())='" + subcategory + "']";
        hoverElement(SelectBy.XPATH, subcategoryXPath + "/../../../preceding-sibling::a"); // TODO XPATH too complex, this will not be robust
                                                                                           // I need to discuss with the developer to add an ID or a class in his HTML
        waitElementIsClickable(SelectBy.XPATH, subcategoryXPath);
        clickElement(SelectBy.XPATH, subcategoryXPath);
    }
}
