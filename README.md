# cucumber-prestashop

Exemple of some Cucumber tests of Pretashop

## Installation of PrestaShop

### Start the containers
See <https://hub.docker.com/r/prestashop/prestashop/>.

```bash
docker network create prestashop-net
docker run -ti --name some-mysql --network prestashop-net -e MYSQL_ROOT_PASSWORD=admin -p 3307:3306 -d mysql:5.7
docker run -ti --name some-prestashop --network prestashop-net -e DB_SERVER=some-mysql -p 8080:80 -d prestashop/prestashop
```

### Configure
Access <http://localhost:8080/> and perform the configuration.  
Then, perform

```bash
docker exec -it some-prestashop rm -fr install
```

### Access to the shop
The shop can be accessed in <http://localhost:8080/>.

### Access to the administration
Perform

```bash
docker exec -it some-prestashop mv admin adminx
```
The shop administration can be accessed in <http://localhost:8080/adminx/>.

## Installation of Geckodriver  
Install Geckodriver (<https://github.com/mozilla/geckodriver>) so it is accessible in your $PATH.

You can also define it from the Maven command line  

```bash
mvn test -Dwebdriver.gecko.driver=/c/Users/lmazure/bin/geckodriver.exe
```
or you can define it in the `pom.xml` file.
